# Author: "Steven F Velez P"
# Email: "steven.velez@unl.edu.ec"

# "Funcion para determinar si un numero es primo o no"

n=int(input("Porfavor ingrese un numero: "))
def determinar_primo(n):
    contador=0
    resultado=True
    for i in range(1,n+1):
        if (n%i==0):
            contador+=1
        if (contador>2):
            resultado=False
            break
    return resultado
if (determinar_primo(n)==True):
    print ("El numero ingresado/leido---> es primo")
else :
    print ("El numero ingresado/leido---> no es primo")